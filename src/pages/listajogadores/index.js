import React from 'react';
import TopBar from '../topBar';
import { connect } from 'react-redux';
import { listarjogadores } from '../../redux/actions/jogadoresactions';
import './listajogadores.css';
import { Link } from 'react-router-dom';


class Listajogadores extends React.Component {


    componentDidMount() {

        this.props.dispatch(listarjogadores())


    }


    render() {

            console.log(this.props)
      

        const { error, loading, jogadores } = this.props;

        

        if(loading){

    return <div>Carregando</div>


        }


        if(error){

            return <div>Erro na requisição</div>


        }


        return (

           
            <div> 
                
            
            <TopBar />
            
                
                <div className= 'container' >
                <div className="row">
                    <div className="col-3"> 
                    
                    </div>
                    <div className='col-9'>

                        <h1 style={{color:'Navy'}}>Lista de Jogadores</h1>
                            
                        <div>

                        <Link to="/jogadores/novo" className="btn btn-dark">Criar um novo jogador</Link>


                        </div>
                            
                            Número de jogadores registrados: {jogadores.length}


                        <table className="table table-Active">
                            <thead>
                                <tr>
                                    <th scope="col">Nome</th>
                                    <th scope="col">Nacionalidade</th>
                                    <th scope="col">Time</th>
                                    <th scope="col">Configurações</th>
                                </tr>
                            </thead>
                            <tbody>
                                {

                                    jogadores.map( (jogador) => (

                                          <tr key={jogador.id}>
                                          <td>{jogador.name}</td>
                                          <td>{jogador.country}</td> 
                                          <td>{jogador.team_name} - {jogador.team_country}</td>
                                           <td>
                                               <Link className="btn btn-secondary" to={{pathname: '/jogadores/' + jogador.id}}>Detalhes</Link>
         
                                           <Link className="btn btn-secondary" to={{ pathname: `/jogadores/editar/${jogador.id}` }} >Editar</Link>
                                           <Link className="btn btn-danger"  >Excluir</Link>
                                            </td> 
                                        </tr>

                                    ))

                                }
                            </tbody>
                        </table>


                    </div>



                </div>

            
            
            </div>
            
            
        </div>

        );



    }



}

function mapStateProps(store) {

    return {
        error: store.jogador.error,
        jogadores: store.jogador.dados,
        loading: store.jogador.loading,
        store


    }



}

export default connect(mapStateProps)(Listajogadores);